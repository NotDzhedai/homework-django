from django.db import models

class Exhibit(models.Model):
  name = models.CharField(max_length=100)
  code = models.CharField(max_length=5)
  description = models.CharField(max_length=200)
  img_url = models.CharField(max_length=100)