from django.shortcuts import render, redirect
from .forms import ExhibitForm
from .models import Exhibit

def exhibit_list(request):
  context = {"exhibit_list":Exhibit.objects.all()}
  return render(request, "main/exhibit_list.html", context)

def exhibit_form(request, id=0):
  if request.method == "GET":
    if id==0:
      form = ExhibitForm()
    else:
      exhibit = Exhibit.objects.get(pk=id)
      form = ExhibitForm(instance=exhibit)
    return render(request, "main/exhibit_form.html", {"form": form})
  else:
    if id==0:
      form = ExhibitForm(request.POST)
    else:
      exhibit = Exhibit.objects.get(pk=id)
      form = ExhibitForm(request.POST, instance=exhibit)
    if form.is_valid():
      form.save()
    return redirect("/")

def exhibit_delete(request, id):  
  exhibit = Exhibit.objects.get(pk=id)
  exhibit.delete()
  return redirect("/")
