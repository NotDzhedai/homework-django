from django import forms
from .models import Exhibit

class ExhibitForm(forms.ModelForm):
  class Meta:
    model = Exhibit
    fields = "__all__"
    labels = {
      "name": "Exhibit name",
      "code": "Exhibit code",
    }
  
