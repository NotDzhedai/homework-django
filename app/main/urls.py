from django.urls import path
from . import views

urlpatterns = [
  path("create/", views.exhibit_form, name="exhibit_insert"),
  path("<int:id>/", views.exhibit_form, name="exhibit_update"),
  path("delete/<int:id>/", views.exhibit_delete, name="exhibit_delete"),
  path("", views.exhibit_list, name="exhibit_list")
]
 